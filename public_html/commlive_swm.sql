-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 12, 2018 at 07:45 AM
-- Server version: 10.0.36-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;

--
-- Database: `commlive_swm`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL DEFAULT '',
  `passWord` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `userName`, `passWord`, `email`) VALUES
(1, 'admin', 'test', 'shsa@sasktel.net');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `action_name` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `position`, `action_name`, `name`, `value`) VALUES
(1, 0, 'Site Name', 'site_name', 'Canadian Herb, Spice and Natural Health Product Coalition'),
(2, 0, 'Contact Email', 'contact_email', 'shsa@sasktel.net'),
(3, 0, 'Site Location', 'site_location', '/home/commlive/public_html/test/'),
(6, 0, 'URL', 'url', 'http://www.communicatelive.com/test/'),
(7, 0, 'Copyright', 'copyright', 'National Herb and Spice Coalition');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(200) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `address` longtext NOT NULL,
  `login` varchar(20) NOT NULL DEFAULT '',
  `pass` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `tax_bracket` varchar(20) NOT NULL DEFAULT '',
  `wage` varchar(5) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `galleryid` int(11) NOT NULL DEFAULT '0',
  `pageid` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(255) NOT NULL DEFAULT '',
  `shortdesc` varchar(255) NOT NULL DEFAULT '',
  `longdesc` longtext NOT NULL,
  `display` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE `library` (
  `id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `date_added` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_emails`
--

CREATE TABLE `newsletter_emails` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(80) NOT NULL DEFAULT '',
  `verification` varchar(10) NOT NULL DEFAULT '',
  `active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(255) NOT NULL DEFAULT '',
  `pos` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `value` longtext NOT NULL,
  `script` longtext NOT NULL,
  `style` longtext NOT NULL,
  `gallery` int(11) NOT NULL DEFAULT '0',
  `display` tinyint(4) NOT NULL DEFAULT '0',
  `page_description` varchar(254) NOT NULL DEFAULT '',
  `page_keywords` varchar(220) NOT NULL DEFAULT '',
  `page_title` varchar(54) NOT NULL DEFAULT '',
  `module_gallery` tinyint(4) NOT NULL DEFAULT '0',
  `module_files` tinyint(4) NOT NULL DEFAULT '0',
  `module_video` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `ref_id`, `pos`, `name`, `description`, `value`, `script`, `style`, `gallery`, `display`, `page_description`, `page_keywords`, `page_title`, `module_gallery`, `module_files`, `module_video`) VALUES
(24, '', 0, 'Home Page', '', '<table width=\"100%\" cellpadding=\"4\" border=\"0\" style=\"border-collapse: collapse;\" id=\"table2\">\r\n    <tbody>\r\n        <tr>\r\n            <td colspan=\"2\"><strong><font face=\"Arial\" color=\"#73a113\">Linking the herb, spice, and natural health products industry from field to shelf.&nbsp; </font></strong></td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan=\"2\"><u><strong><font face=\"Arial\" color=\"#73a113\"><br />\r\n            VISION</font></strong></u> <br />\r\n            <br />\r\n            <font size=\"2\" face=\"Arial\">A profitable and sustainable Canadian herb, spice and natural health product industry. </font>\r\n            <p><u><strong><font face=\"Arial\" color=\"#73a113\">MISSION</font></strong></u><font face=\"Arial\"> <br />\r\n            <br />\r\n            B<font size=\"2\">uild a viable national herb, spice and natural health product industry which highlights regional strengths and expertise and optimizes national synergies. </font></font></p>\r\n            <u><strong><font face=\"Arial\" color=\"#73a113\">GOALS</font></strong></u><font face=\"Arial\"> </font>\r\n            <ul>\r\n                <li><font size=\"2\" face=\"Arial\">Lead the development and support the growth of the industry. </font> </li>\r\n                <li><font size=\"2\" face=\"Arial\">Build value chains whereby there is increased cooperation amongst industry. participants and increased net return across the industry. </font> </li>\r\n                <li><font size=\"2\" face=\"Arial\">Support provincial associations and address overarching national issues. </font></li>\r\n            </ul>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td colspan=\"2\"><u><strong><font face=\"Arial\" color=\"#73a113\">ROLE OF THE COALITION </font></strong></u></td>\r\n        </tr>\r\n        <tr>\r\n            <td><font size=\"2\" face=\"Arial\"><strong>1.</strong> <strong>Coordinate research and funding </strong></font>\r\n            <ul>\r\n                <li><font size=\"2\" face=\"Arial\">Provide direction from an industry standpoint to the research community and avoid duplication.</font>  </li>\r\n                <li><font size=\"2\" face=\"Arial\">Initiate research in the area of applied science. </font></li>\r\n            </ul>\r\n            </td>\r\n            <td valign=\"top\"><font size=\"2\" face=\"Arial\"><strong>2.</strong> <strong>Communication</strong></font>\r\n            <ul>\r\n                <li><font size=\"2\" face=\"Arial\">Provide a forum for industry to exchange ideas and to do business. </font> </li>\r\n                <li><font size=\"2\" face=\"Arial\">Provide education and training from field to shelf. </font></li>\r\n            </ul>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td valign=\"top\"><font size=\"2\" face=\"Arial\"><strong>3. Voice to Government </strong></font>\r\n            <ul>\r\n                <li><font size=\"2\" face=\"Arial\">Ensure national representation of the industry to all levels of government. </font> </li>\r\n                <li><font size=\"2\" face=\"Arial\">Encourage and support proactive dialogue between industry and government. </font></li>\r\n            </ul>\r\n            <p>&nbsp;</p>\r\n            </td>\r\n            <td><font size=\"2\" face=\"Arial\"><strong>4.</strong> <strong>Marketing</strong> </font>\r\n            <ul>\r\n                <li><font size=\"2\" face=\"Arial\">Promote and heighten awareness of the industry in Canada. </font> </li>\r\n                <li><font size=\"2\" face=\"Arial\">Promote Canadian standards on the international market in the context of branding Canada.</font> </li>\r\n            </ul>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>', '', '', 0, 0, '', '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `complete_date` varchar(20) NOT NULL DEFAULT '',
  `completed` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `task_notes`
--

CREATE TABLE `task_notes` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL DEFAULT '0',
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `cpp` varchar(10) NOT NULL DEFAULT '',
  `ei` varchar(10) NOT NULL DEFAULT '',
  `fit` varchar(10) NOT NULL DEFAULT '',
  `pit` varchar(10) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `times`
--

CREATE TABLE `times` (
  `id` int(11) NOT NULL,
  `mem_id` int(11) NOT NULL DEFAULT '0',
  `start_time` varchar(20) NOT NULL DEFAULT '',
  `end_time` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ultra_admin_settings`
--

CREATE TABLE `ultra_admin_settings` (
  `id` int(11) NOT NULL,
  `pos` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ultra_admin_settings`
--

INSERT INTO `ultra_admin_settings` (`id`, `pos`, `name`, `value`) VALUES
(1, 0, 'style', 'no'),
(2, 0, 'script', 'no'),
(3, 0, 'form', 'no'),
(4, 2, 'files', 'no'),
(5, 0, 'banner', 'no'),
(6, 1, 'gallery', 'no'),
(7, 3, 'video', 'no'),
(8, 0, 'products', 'no'),
(9, 0, 'page_add', 'no'),
(10, 0, 'page_delete', 'no'),
(11, 0, 'page_position', 'no'),
(12, 0, 'sub_page_add', 'no'),
(13, 0, 'sub_page_delete', 'no'),
(14, 0, 'sub_page_position', 'no'),
(15, 0, 'news', 'no'),
(16, 0, 'employees', 'no'),
(17, 0, 'timesheet', 'no'),
(18, 0, 'tasks', 'no'),
(19, 0, 'sub_page', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `vidfile` varchar(255) NOT NULL DEFAULT '',
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter_emails`
--
ALTER TABLE `newsletter_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_notes`
--
ALTER TABLE `task_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `times`
--
ALTER TABLE `times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ultra_admin_settings`
--
ALTER TABLE `ultra_admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `newsletter_emails`
--
ALTER TABLE `newsletter_emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `task_notes`
--
ALTER TABLE `task_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `times`
--
ALTER TABLE `times`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ultra_admin_settings`
--
ALTER TABLE `ultra_admin_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
