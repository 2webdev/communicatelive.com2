<?php include('adminlogin.php'); ?>

<?php $fnct = $_REQUEST['fnct'];

if($fnct == '')
{
	$fnct = "home";
}

$section_name = "Clients";
?>

<?php include("includes/head.php"); ?>

<?php

function category($site_name,$section_name)
{
	echo "<table width='750' cellpadding='0' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">
			<tr>
				<th>&nbsp;" . $site_name . " >> " . $section_name . "</th>
			</tr>
			<tr class='tablerow1'>
				<td align='left'>&nbsp;<a href='clients.php'>Clients Home</a></td>
			</tr>
		</table><br />";

}


function home($site_name,$section_name)
{
category($site_name,$section_name);
	echo "
	<table width='750' cellpadding='0' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">
		<tr>
			<td><form action='clients.php?fnct=add_client' method='post'><input type='submit' value='Add Client' /></form></td>
		</tr>
		<tr>
			<td width='120'><strong>&nbsp;Image</strong></td><td width='120'><strong>Pos</strong></td><td><strong>Name</strong></td><td><strong>Edit</strong></td><td><strong>Delete</strong></td>
		</tr>";
	$query = "SELECT * FROM clients ORDER BY pos";
	$result = @mysql_query($query);
	while($row = @mysql_fetch_array($result))
	{
		echo "
		<tr>
			<td><a href='../clients/" . $row['big_pic'] . "' target='_blank'><img src='../clients/" . $row['small_pic'] . "' border='0' alt='' /></a></td>
			<td><form action='clients.php?fnct=pos_client&amp;id=" . $row['id'] . "' method='post'><input type='text' name='pos' value='" . $row['pos'] . "' size='2' /><input type='submit' value='Save' /></form></td>
			<td>" . $row['name'] . "</td>
			<td><a href='clients.php?fnct=edit_client&amp;id=" . $row['id'] . "'>Edit</a></td>
			<td><a href='clients.php?fnct=delete_client&amp;id=" . $row['id'] . "'>Delete</a></td>
		</tr>";
	}
	
	echo "</table>
	
	";
}

function pos_client($site_name,$section_name)
{
	$id = $_REQUEST['id'];
	$pos = $_REQUEST['pos'];
	
	$query = "UPDATE clients SET pos = '$pos' WHERE id = $id";
	$result = @mysql_query($query);
	header("location:clients.php?fnct=home");
}

function add_client($site_name,$section_name)
{
	category($site_name,$section_name);
	echo "
	<form action='clients.php?fnct=add_client_complete' method='post' enctype='multipart/form-data'>
	<table width='750' cellpadding='0' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">
		<tr class='tablerow2'>
			<td align='right'><b>Name:</b></td>
			<td align='left'><input type='text' name='name' maxlength='255' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='right'><b>URL</b></td>
			<td align='left'><input type='text' name='url' maxlength='255' /></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><b>Description</b></td>
			<td align='left'><input type='text' name='description' maxlength='255' value='" . $row['description'] . "' size='100' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='right'><b>Small Pic</b></td>
			<td align='left'><input type='file' name='upload[]' /></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><b>Big Pic</b></td>
			<td align='left'><input type='file' name='upload[]' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='center' colspan='2'><input type='submit' value='Submit' /></td>
		</tr>
	
	</table>
	</form>	
	";
}

function add_client_complete($site_name,$section_name)
{
	$name = $_REQUEST['name'];
	$url = $_REQUEST['url'];
	$description = $_REQUEST['description'];

	$query = "SELECT value FROM config WHERE name = 'site_location'";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	$site_location = $row['value'];

	for($i = 0; $i < count($_FILES['upload']); $i++) 
	{
		if ($_FILES['upload']['error'][$i] == UPLOAD_ERR_OK) 
		{
			$tempName = $_FILES['upload']['tmp_name'][$i];
			$fileName = $_FILES['upload']['name'][$i];
			$saveDirectory = $site_location . "clients/";
			@move_uploaded_file($tempName, $saveDirectory . $fileName);
		} 
		else
		{
			$error = "error error<br />";
		}
	}
	
	$small_pic = $_FILES['upload']['name'][0];
	$big_pic = $_FILES['upload']['name'][1];
	
	$query = "INSERT INTO clients
		SET
			name = '$name',
			url = '$url',
			description = '$description',
			small_pic = '$small_pic',
			big_pic = '$big_pic'";
	$result = @mysql_query($query);
	header("Location:clients.php");


	
}

function edit_client($site_name,$section_name)
{
	category($site_name,$section_name);
	$id = $_REQUEST['id'];
	$query = "SELECT * FROM clients WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	
	echo "
	<form action='clients.php?fnct=edit_client_complete&amp;id=$id' method='post' enctype='multipart/form-data'>
	<table width='750' cellpadding='0' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">
		<tr class='tablerow2'>
			<td align='right'><b>Name:</b></td>
			<td align='left'><input type='text' name='name' maxlength='255' value='" . $row['name'] . "' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='right'><b>URL</b></td>
			<td align='left'><input type='text' name='url' maxlength='255' value='" . $row['url'] . "'  /></td>
		</tr>
		
		<tr class='tablerow2'>
			<td align='right'><b>Description</b></td>
			<td align='left'><input type='text' name='description' maxlength='255' value='" . $row['description'] . "' size='100' /></td>
		</tr>
		
		<tr class='tablerow1'>
			<td align='right' valign='top'><b>Current Small Pic:</b></td>
			<td align='left'><a href='../clients/" . $row['small_pic'] . "' target='_blank'><img src='../clients/" . $row['small_pic'] . "' alt='' border='0' width='100' /></a> <input type='text' name='image' value='" . $row['small_pic'] . "' size='40' /></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><b>Change Small Pic:</b> </td>
			<td align='left'><input type='file' name='upload[]' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='right' valign='top'><b>Current Big Pic:</b></td>
			<td align='left'><a href='../clients/" . $row['big_pic'] . "' target='_blank'><img src='../clients/" . $row['big_pic'] . "' alt='' border='0' width='100' /></a> <input type='text' name='image' value='" . $row['big_pic'] . "' size='40' /></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><b>Big Pic</b></td>
			<td align='left'><input type='file' name='upload[]' /></td>
		</tr>
		<tr class='tablerow1'>
			<td align='center' colspan='2'><input type='submit' value='Submit' /></td>
		</tr>
	
	</table>
	</form>	
	";
}

function edit_client_complete($site_name,$section_name)
{
	$id = $_REQUEST['id'];
	$name = $_REQUEST['name'];
	$url = $_REQUEST['url'];
	$description = $_REQUEST['description'];
	
	$query = "UPDATE clients
		SET
			name = '$name',
			url = '$url',
			description = '$description'
		WHERE
			id = $id";
	$result = @mysql_query($query);
	header("Location:clients.php");
	
}

function delete_client($site_name,$section_name)
{
	category($site_name,$section_name);
	$id = $_REQUEST['id'];
	echo "
	<table width='750' cellpadding='0' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">
		<tr class='tablerow1'>
			<td colspan='3'>Are you sure you want to delete this Client <u><b>forever</b></u></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><form action='clients.php?fnct=delete_client_complete&amp;id=$id' method='post'><input type='submit' value='Yes' /></form></td>
			<td align='left'><form action='clients.php' method='post'><input type='submit' value='No' /></form></td>
			<td width='350'>&nbsp;</td>
		</tr>
	</table>";

}

function delete_client_complete($site_name,$section_name)
{
	$id = $_REQUEST['id'];
	$query = "DELETE FROM clients WHERE id = $id";
	$result = @mysql_query($query);
	header("Location:clients.php");
}
?>

<?php include("includes/header.php"); ?>

	<?php

switch($fnct)
{
	case 'add_client':
	add_client($site_name,$section_name);
	break;
	
	case 'add_client_complete':
	add_client_complete($site_name,$section_name);
	break;
	
	case 'edit_client':
	edit_client($site_name,$section_name);
	break;
	
	case 'edit_client_complete':
	edit_client_complete($site_name,$section_name);	
	break;

	case 'delete_client':
	delete_client($site_name,$section_name);
	break;
	
	case 'delete_client_complete':
	delete_client_complete($site_name,$section_name);
	break;

	case 'pos_client':
	pos_client($site_name,$section_name);
	break;

	default:
	home($site_name,$section_name);
	break;

}	

?>

<?php include("includes/footer.php"); ?>
