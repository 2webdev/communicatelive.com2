<?php include('adminlogin.php'); ?>

<?php

$fnct = $_REQUEST['fnct'];

if($fnct == '')
{	
	$fnct = "home";
}

$section_name = "Staff";
?>
<?php include("includes/head.php"); ?>

<?php
function middle_top()
{
	echo "<table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"15\"><img src=\"table/top_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td width=\"510\" style=\"background-image:url('table/top.gif');\"></td>
            <td width=\"15\"><img src=\"table/top_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
          <tr>
            <td width=\"15\" style=\"background-image:url('table/left.gif');\"></td>
            <td bgcolor=\"#F9F9F9\">";
}

function middle_bottom()
{
	echo "
				<p>&nbsp;</p>
			</td>
            <td width=\"15\" style=\"background-image:url('table/right.gif');\"></td>
          </tr>
          <tr>
            <td><img src=\"table/bottom_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td style=\"background-image:url('table/bottom.gif');\"></td>
            <td><img src=\"table/bottom_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
        </table>";
}



function home($ultra_timesheet,$ultra_tasks)
{
	
	middle_top();
	
	$query = "SELECT * FROM employee";
	$result = @mysql_query($query);
	echo "
	<table>
		<tr>
			<td colspan='2' align='left'><form action='staff.php?fnct=add_staff' method='post'><input type='submit' value='Add Member' /></form></td>
		</tr>
		<tr>
			<td><strong>Name</strong></td>";
		if($ultra_timesheet == "yes")
		{
		echo "
			<td><strong>Logged In?</strong></td>";
		}
		if($ultra_tasks == "yes")
		{
		echo "
			<td><strong>Tasks</strong></td>";
		}
		if($ultra_timesheet == "yes")
		{
		echo "
			<td><strong>Times</strong></td>";
		}
	echo	"
			<td><strong>Edit</strong></td>
			<td><strong>Delete</strong></td>
		</tr>";
	
	
	while($row = @mysql_fetch_array($result))
	{
	echo 
		"<tr>
			<td>" . $row['name'] . "</td>";
	
		if($ultra_timesheet == "yes")
		{
			echo "
			<td>";
			if ($row['status'] == '1')
			{
				echo "Logged In";
			}
			else
			{
				echo "Logged Out";
			}
			echo "
			</td>";
		}
		if($ultra_tasks == "yes")
		{
			echo "
			<td><a href='tasks.php?fnct=view_task&amp;id=" . $row['id'] . "'>Tasks</a></td>";
		}
		if($ultra_timesheet == "yes")
		{
			echo "	
			<td><a href='calc.php?fnct=time_worked&amp;id=" . $row['id'] . "'>Times</a></td>";
		}
			echo "
			<td><a href='staff.php?fnct=edit_staff&amp;id=" . $row['id'] . "'>Edit</a></td>
			<td><a href='staff.php?fnct=delete_staff&amp;id=" . $row['id'] . "'>Delete</a></td>
		
		</tr>";
	
	
	}
	echo "
	</table>";
	
	middle_bottom();
	
}

function add_staff()
{
	middle_top();
	
	echo "
	<form action='staff.php?fnct=add_staff_complete' method='post'>
	<table>
		<tr>
			<td align='right'><strong>Name:</strong></td><td align='left'><input type='text' name='name' size='20' maxlength='200' /></td>
			<td align='right'><strong>Email:</strong></td><td align='left'><input type='text' name='email' size='20' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Address:</strong></td><td align='left' colspan='3'><input type='text' name='address' size='40' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Wage:</strong></td><td align='left'><input type='text' name='wage' size='20' maxlength='5' /></td>
			<td align='right'><strong>Tax Bracket:</strong></td><td align='left'>";
		$query = "SELECT * FROM taxes ORDER BY id ASC";
		$result = @mysql_query($query);
			echo "<select name='tax'>";
		while($row = @mysql_fetch_array($result))
		{
			echo "<option value='" . $row['id'] . "'>" . $row['name'] ."</option>";
		
		}
			echo "</select>";
	echo 	
			"</td>
		</tr>
		<tr>
			<td align='right'><strong>Login:</strong></td><td align='left'><input type='text' name='login' size='20' maxlength='20' /></td>
			<td align='right'><strong>Password:</strong></td><td align='left'><input type='password' name='pass' size='20' maxlength='20' /></td>
		</tr>
		<tr>
			<td colspan='4' align='center'><input type='submit' value='Add' /></td>
		</tr>	
	</table>
	</form>	
	";
	
	middle_bottom();
}

function add_staff_complete()
{
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$address = $_REQUEST['address'];
	$login = $_REQUEST['login'];
	$pass = $_REQUEST['pass'];
	$wage = $_REQUEST['wage'];
	$tax = $_REQUEST['tax'];
	
	$query = "INSERT INTO employee
		SET
			name = '$name',
			email = '$email',
			address = '$address',
			login = '$login',
			pass = '$pass',
			tax_bracket = '$tax',
			wage = '$wage'";
	$result = @mysql_query($query);
	header("Location:staff.php");

}

function edit_staff()
{
	middle_top();
	
	$id = $_REQUEST['id'];
	$query = "SELECT * FROM employee WHERE id=$id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	
	echo "
	<form action='staff.php?fnct=edit_staff_complete&amp;id=$id' method='post'>
	<table>
		<tr>
			<td align='right'><strong>Name:</strong></td><td align='left'><input type='text' name='name' size='20' maxlength='200' value='" . $row['name'] . "' /></td>
			<td align='right'><strong>Email:</strong></td><td align='left'><input type='text' name='email' size='20' maxlength='200' value='" . $row['email'] . "' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Address:</strong></td><td align='left' colspan='3'><input type='text' name='address' size='40' maxlength='200' value='" . $row['address'] . "' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Wage:</strong></td><td align='left'><input type='text' name='wage' size='20' maxlength='5' value='" . $row['wage'] . "' /></td>
			<td align='right'><strong>Tax Bracket:</strong></td><td align='left'>";
		$query = "SELECT * FROM taxes ORDER BY id ASC";
		$result = @mysql_query($query);
			echo "<select name='tax'>";
		while($row_tax = @mysql_fetch_array($result))
		{
			if ($row_tax['id'] == $row['tax_bracket'])
			{
				echo "<option value='" . $row_tax['id'] . "' SELECTED>" . $row_tax['name'] ."</option>";
			}
			else
			{
				echo "<option value='" . $row_tax['id'] . "'>" . $row_tax['name'] ."</option>";
			}
		}
			echo "</select>";
	echo 	
			"</td>
		
		<tr>
			<td align='right'><strong>Login:</strong></td><td align='left'><input type='text' name='login' size='20' maxlength='20' value='" . $row['login'] . "' /></td>
			<td align='right'><strong>Password:</strong></td><td align='left'><input type='password' name='pass' size='20' maxlength='20' value='" . $row['pass'] . "' /></td>
		</tr>
		<tr>
			<td colspan='4' align='center'><input type='submit' value='Save' /></td>
		</tr>	
	</table>
	</form>	
	";
	
	middle_bottom();
	
}

function edit_staff_complete()
{
	$id = $_REQUEST['id'];
	$name = $_REQUEST['name'];
	$email = $_REQUEST['email'];
	$address = $_REQUEST['address'];
	$login = $_REQUEST['login'];
	$pass = $_REQUEST['pass'];
	$wage = $_REQUEST['wage'];
	$tax = $_REQUEST['tax'];

	$query = "UPDATE employee
		SET
			name = '$name',
			email = '$email',
			address = '$address',
			login = '$login',
			pass = '$pass',
			tax_bracket = '$tax',
			wage = '$wage'
		WHERE
			id = $id";
	$result = @mysql_query($query);
	header("Location:staff.php");
}

function delete_staff()
{
	middle_top();
	
	$id = $_REQUEST['id'];
	echo "
	<table>
		<tr>
			<td colspan='2' align='center'>Are you sure you would like to delete the staff member <u>forever</u>?</td>
		</tr>
		<tr>
			<td align='left'><form action='staff.php?fnct=delete_staff_complete&amp;id=$id' method='post'><input type='submit' value='Yes' /></form></td>
			<td align='right'><form action='staff.php' method='post'><input type='submit' value='No' /></form></td>
		</tr>
	</table>";
	
	middle_bottom();
	
}

function delete_staff_complete()
{
	$id = $_REQUEST['id'];
	$query = "DELETE FROM employee 
		WHERE 
			id = $id";
	$result = @mysql_query($query);
	header("Location:staff.php");
}

?>

<?php include("includes/header.php"); ?>
<table width="750" border="0" cellpadding="0" cellspacing="10" style="height:400px;">
      <tr>
        <td width="200" valign="top">
		<table width="189" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td><img src="images/top_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
          <tr>
            <td bgcolor="#F6FAFE" style="border-right:1px solid #C2DDFA;border-left:1px solid #C2DDFA;padding-left:20px;padding-right:20px;" valign="top"> 
			<a href='staff.php'>Staff List</a><br />
			<a href="staff.php?fnct=add_staff">Add Staff</a><br />
			<?php
			if($ultra_timesheet == "yes")
			{
			?>
			<a href="tax_bracket.php">Taxes</a><br />
			<?php
			}
			?>

			
			</td>
          </tr>
          <tr>
            <td><img src="images/bottom_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
        </table>
		</td>
        <td valign="top">


<?php

switch($fnct)
{
	case 'add_staff':
	add_staff();
	break;

	case 'add_staff_complete':
	add_staff_complete();
	break;
	
	case 'edit_staff':
	edit_staff();
	break;
	
	case 'edit_staff_complete':
	edit_staff_complete();
	break;	

	case 'delete_staff':
	delete_staff();
	break;
	
	case 'delete_staff_complete':
	delete_staff_complete();
	break;	

	default:
	home($ultra_timesheet,$ultra_tasks);
	break;
}

?>
		</td>
      </tr>
    </table>
<?php include("includes/footer.php"); ?>