<?php ob_start();
include_once 'common.php';
include_once 'config.php';

session_start();

$uiduid = isset($_POST['uiduid']) ? $_POST['uiduid'] : $_SESSION['uiduid'];
$pwdpwd = isset($_POST['pwdpwd']) ? $_POST['pwdpwd'] : $_SESSION['pwdpwd'];

if(!isset($uiduid)) {

//include 'includes/header.php';
  ?>
<table width="100%">
	<tr>
		<td valign="top">
			<h2> Welcome to the <?php echo $site_name ?> Admin Panel </h2>
			<p>You must log in to access this area of the site. If you are
			not a registered user, please contact the administrator for access.</p>
			<p><form method="post" action="<?=$_SERVER['PHP_SELF']?>">
			User ID: <input type="text" name="uiduid" size="8" /><br />
			Password: <input type="password" name="pwdpwd" SIZE="8" /><br />
			<input type="submit" value="Log in" />
			</form></p>
			<p><a href='forgot_password.php'>Forgot Password</a></p>
		</td>
	</tr>
</table>  
<?php
//include 'includes/footer.php';
  exit;
}

$_SESSION['uiduid'] = $uiduid;
$_SESSION['pwdpwd'] = $pwdpwd;

function quote_smart($value)
{
   // Stripslashes
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   // Quote if not a number or a numeric string
   if (!is_numeric($value)) {
       $value = "'" . mysql_real_escape_string($value) . "'";
   }
   return $value;
}
$query = sprintf("SELECT * FROM admin WHERE userName = %s AND passWord=%s",
		   quote_smart($uiduid),
           quote_smart($pwdpwd));

//$sql = "SELECT * FROM admin WHERE
//        userName = '$uiduid' AND passWord = '$pwdpwd'";
$result = mysql_query($query);
if (!$result) {
  error('A database error occurred while checking your '.
        'login details.\\nIf this error persists, please '.
        'contact an administrator.');
}

if (mysql_num_rows($result) == 0) {
  unset($_SESSION['uiduid']);
  unset($_SESSION['pwdpwd']);
//include 'includes/header.php';
  ?>
<table width="100%">
	<tr>
		<td valign="top">
			<h2> Access Denied </h2>
			<p>Your user ID or password is incorrect, or you are not a
			registered user on this site. To try logging in again, click
			<a href="<?=$_SERVER['PHP_SELF']?>">here</a></p>
			<p><a href='forgot_password.php'>Forgot Password</a></p>
		</td>
	</tr>
</table> 
<?php
//include 'includes/footer.php';
  exit;
}

$username = mysql_result($result,0,'userName');
?>
