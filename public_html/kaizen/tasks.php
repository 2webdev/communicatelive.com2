<?php include('adminlogin.php'); ?>

<?php

$fnct = $_REQUEST['fnct'];

if($fnct == '')
{	
	$fnct = "home";
}

$section_name = "Staff";
?>
<?php include("includes/head.php"); ?>

<?php
function middle_top()
{
	echo "<table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"15\"><img src=\"table/top_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td width=\"510\" style=\"background-image:url('table/top.gif');\"></td>
            <td width=\"15\"><img src=\"table/top_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
          <tr>
            <td width=\"15\" style=\"background-image:url('table/left.gif');\"></td>
            <td bgcolor=\"#F9F9F9\">";
}

function middle_bottom()
{
	echo "
				<p>&nbsp;</p>
			</td>
            <td width=\"15\" style=\"background-image:url('table/right.gif');\"></td>
          </tr>
          <tr>
            <td><img src=\"table/bottom_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td style=\"background-image:url('table/bottom.gif');\"></td>
            <td><img src=\"table/bottom_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
        </table>";
}



function home()
{
	echo "
	<form action='tasks.php?fnct=add_task' method='post'><input type='submit' value='Add Task' /></form>";

	$emp_query = "SELECT * FROM employee";
	$emp_result = @mysql_query($emp_query);
	while($row_emp = @mysql_fetch_array($emp_result))
	{
		$employee[$row_emp['id']] = $row_emp['name'];	
	}	
	
	middle_top();

	$query = "SELECT * FROM tasks WHERE completed = 0 ORDER BY id";
	$result = @mysql_query($query);
	echo "
	<table width='100%'>
		<tr>
			<td><strong>Staff</strong></td>
			<td><strong>Title</strong></td>
			<td><strong>View</strong></td>
			<td><strong>Edit</strong></td>
			<td><strong>Delete</strong></td>
		</tr>";
	while($row = @mysql_fetch_array($result))
	{
		echo "
		<tr>
			<td>";
				echo $employee[$row['emp_id']];
		echo "
			</td>
			<td>" . $row['title'] . "</td>
			<td><a href='tasks.php?fnct=view_task_content&amp;id=" . $row['id'] . "'>View</a></td>
			<td><a href='tasks.php?fnct=edit_task&amp;id=" . $row['id'] . "'>Edit</a></td>
			<td>[ <a href='tasks.php?fnct=delete_task&amp;id=" . $row['id'] . "' style=\"color:#CC0000;\">X</a> ]</td>
		</tr>";	
	}
	echo "
	</table>";

	middle_bottom();
	
}

function view_task()
{
	$id = $_REQUEST['id'];

	echo "
	<form action='tasks.php?fnct=add_task&amp;id=$id' method='post'><input type='submit' value='Add Task' /></form>";
	
	middle_top();

	$query = "SELECT * FROM tasks WHERE emp_id = $id AND completed = 0 ORDER BY id";
	$result = @mysql_query($query);
	echo "
	<table>
		<tr>
			<td><strong>Title</strong></td>
			<td><strong>View</strong></td>
			<td><strong>Edit</strong></td>
			<td><strong>Delete</strong></td>
		</tr>";
	while($row = @mysql_fetch_array($result))
	{
		echo "
		<tr>
			<td>" . $row['title'] . "</td>
			<td><a href='tasks.php?fnct=view_task_content&amp;id=" . $row['id'] . "&amp;emp_id=$id'>View</a></td>
			<td><a href='tasks.php?fnct=edit_task&amp;id=" . $row['id'] . "&amp;emp_id=$id'>Edit</a></td>
			<td>[ <a href='tasks.php?fnct=delete_task&amp;id=" . $row['id'] . "&amp;emp_id=$id' style=\"color:#CC0000;\">X</a> ]</td>
		</tr>";	
	}
	echo "
	</table>";

	middle_bottom();
	
}

function complete_tasks()
{
	middle_top();

	$query = "SELECT * FROM tasks WHERE completed = 1";
	$result = @mysql_query($query);
echo "
	<table>";
	while($row = @mysql_fetch_array($result))
	{
	echo "
		<tr>
			<td><strong>" . $row['title'] . "</strong></td>
			<td>Completed: " . date('H:i d-m-Y',mktime(date('H',$row['complete_date']),date('i',$row['complete_date']),0,date('m',$row['complete_date']),date('d',$row['complete_date']),date('Y',$row['complete_date']))) . "</td>
			<td><a href='tasks.php?fnct=view_completed_task&amp;id=" . $row['id'] . "&amp;emp_id=" . $row['emp_id'] . "'>View</a></td>
		</tr>\n";	
	}

echo "
	</table>";
	middle_bottom();
}

function view_completed_task()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];

	middle_top();

	$query = "SELECT * FROM tasks WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
		echo "
		<table width='750' cellpadding='3' cellspacing='0' style=\"border-width:thin; border-style:solid; border-color:black;\">";
		echo "
			<tr>
				<td colspan='3' bgcolor='#cecece'>
					<strong>" . $row['title'] . "</strong>";
		echo	"</td>
			</tr>
			<tr>
				<td colspan='3' bgcolor='#ffffcc'>
					" . $row['description'] . "
				</td>
			</tr>";

		$query_notes = "SELECT * FROM task_notes WHERE task_id = " . $row['id'] . " ORDER BY id";
		$result_notes = @mysql_query($query_notes);

		$bgcolor = "#eeeeee";
		while($row_notes = @mysql_fetch_array($result_notes))
		{
			if ($bgcolor == "#ececec")
			{
				$bgcolor = "#eeeeee";
			}
			else
			{
				$bgcolor = "#ececec";
			}
			$p_time = $row_notes['posted_time'];
			$posted_time = date("H:i d-m-Y", mktime(date("H",$p_time),date("i",$p_time),0,date("m",$p_time),date("d",$p_time),date("Y",$p_time)));
		echo "
			<tr>
				<td colspan='3'><hr /></td>
			</tr>
			<tr bgcolor='$bgcolor'>
				<td>Posted by: " . $row_notes['posted_by'] . " At: " . $posted_time . " <br />" . $row_notes['value'] . "</td>
				<td width='50' valign='top'><a href='tasks.php?fnct=edit_note&amp;id=" . $row_notes['id'] ."&amp;task_id=" . $row['id'] . "'>Edit</a></td>
				<td width='50' valign='top'>[ <a href='tasks.php?fnct=delete_note&amp;id=" . $row_notes['id'] ."&amp;task_id=" . $row['id'] . "' style='color:#ff0000;'>X</a> ]</td>
			</tr>";
		}	
		echo "
			<tr>
				<td colspan='3'><hr /></td>
			</tr>
			<tr>
				<td colspan='3'>Add Note:<br /><form action='tasks.php?fnct=add_note_complete&amp;id=$id' method='post' onsubmit='return submitForm();'>";
	$query = "SELECT name FROM admin";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	
	echo "<input type='hidden' name='posted_by' value='" . $row['name'] . "' />";
		?>
				<script language="JavaScript" type="text/javascript">
					<!--
					function submitForm() {
					//make sure hidden and iframe values are in sync before submitting form
					//to sync only 1 rte, use updateRTE(rte)
					//to sync all rtes, use updateRTEs
					updateRTE('value');
					//updateRTEs();
					//change the following line to true to submit form
					return true;
					}
					//Usage: initRTE(imagesPath, includesPath, cssFile, genXHTML)
					initRTE("images/", "", "", true);
					//-->
					</script><noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>
		<?php
		echo "<script language='JavaScript' type='text/javascript'>";
		//format content for preloading
		/*if (!(isset($row['info']))) {
			$content = "here's the " . chr(13) . "\"preloaded <b>content</b>\"";
			$content = rteSafe($content);
		} else {
			//retrieve posted value
			*/
			//$content = rteSafe($row['value']);
		//}
		//Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
		?>
					
		writeRichText('value', '<?=$content;?>', 530, 330, true, false);
		</script><?php echo "<br /><input type='submit' value='Add Note' /></form></td>
			</tr>
		</table>";

	middle_bottom();
}


function view_task_content()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];

	middle_top();


	$query = "SELECT * FROM tasks WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);

		echo "<strong>" . $row['title'] . "</strong><br />" . $row['description'];
		$query_notes = "SELECT * FROM task_notes WHERE task_id = " . $row['id'] . " ORDER BY id";
		$result_notes = @mysql_query($query_notes);
		echo "
		<table width='100%' cellspacing='0'>";
		$bgcolor = "#eeeeee";
		while($row_notes = @mysql_fetch_array($result_notes))
		{
			if ($bgcolor == "#ececec")
			{
				$bgcolor = "#eeeeee";
			}
			else
			{
				$bgcolor = "#ececec";
			}

		echo "
			<tr>
				<td colspan='3'><hr /></td>
			</tr>
			<tr bgcolor='$bgcolor'>
				<td>" . $row_notes['value'] . "</td>
				<td width='50'><a href='tasks.php?fnct=edit_note&amp;id=" . $row_notes['id'] ."&amp;task_id=" . $row['id'] . "'>Edit</a></td>
				<td width='50'>[ <a href='tasks.php?fnct=delete_note&amp;id=" . $row_notes['id'] ."&amp;task_id=" . $row['id'] . "' style='color:#ff0000;'>X</a> ]</td>
			</tr>";
		}	
		echo "
			<tr>
				<td colspan='3'><hr /></td>
			</tr>
			<tr>
				<td colspan='3'>Add Note:<br /><form action='tasks.php?fnct=add_note_complete&amp;id=$id' method='post' onsubmit='return submitForm();'>";?>
				<script language="JavaScript" type="text/javascript">
					<!--
					function submitForm() {
					//make sure hidden and iframe values are in sync before submitting form
					//to sync only 1 rte, use updateRTE(rte)
					//to sync all rtes, use updateRTEs
					updateRTE('value');
					//updateRTEs();
					//change the following line to true to submit form
					return true;
					}
					//Usage: initRTE(imagesPath, includesPath, cssFile, genXHTML)
					initRTE("images/", "", "", true);
					//-->
					</script><noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>
		<?php
		echo "<script language='JavaScript' type='text/javascript'>";
		//format content for preloading
		/*if (!(isset($row['info']))) {
			$content = "here's the " . chr(13) . "\"preloaded <b>content</b>\"";
			$content = rteSafe($content);
		} else {
			//retrieve posted value
			*/
			//$content = rteSafe($row['value']);
		//}
		//Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
		?>
					
		writeRichText('value', '<?=$content;?>', 530, 330, true, false);
		</script><?php echo "<br /><input type='submit' value='Add Note' /></form></td>
			</tr>
		</table>";

	
	middle_bottom();

}


function add_task()
{
	$id = $_REQUEST['id'];
		//get a drop down for employees
	$query = "SELECT * FROM employee";
	$result = @mysql_query($query);
	$employees = "
		<tr>
			<td align='right'><strong>Employee:</strong></td><td align='left'><select name='emp_id'>";
		while($row = @mysql_fetch_array($result))
		{
			if ($row['id'] == $id)
			{
				$employees .= "<option value='" . $row['id'] . "' SELECTED>" . $row['name'] . "</option>";
			}
			else
			{
				$employees .= "<option value='" . $row['id'] . "'>" . $row['name'] . "</option>";
			}
		}
	$employees .= "
				</select></td>
		</tr>";

	middle_top();
	echo "
	<form action='tasks.php?fnct=add_task_complete' method='post'>
	<table>
		$employees
		<tr>
			<td align='right'><strong>Title:</strong></td><td align='left'><input type='text' name='title' maxlength='255' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Description:</strong></td><td align='left'><textarea cols='30' rows='3' name='description'></textarea></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Add' /></td>
		</tr>
	</table>
	</form>";

	middle_bottom();
	
}

function add_task_complete()
{
	$emp_id = $_REQUEST['emp_id'];
	$title = $_REQUEST['title'];
	$description = $_REQUEST['description'];
	
	 $query = "INSERT INTO tasks
	 	SET
			emp_id = '$emp_id',
			title = '$title',
			description = '$description'";
	$result = @mysql_query($query);
	header("Location: tasks.php?fnct=view_task&id=$emp_id");
}

function edit_task()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];
	
	$employee_query = "SELECT * FROM employee";
	$employee_result = @mysql_query($employee_query);
	$employees = "
		<tr>
			<td align='right'><strong>Employee:</strong></td><td align='left'><select name='emp_id'>";
	while($employee_row = @mysql_fetch_array($employee_result))
	{
			if ($employee_row['id'] == $emp_id)
			{
				$employees .= "<option value='" . $employee_row['id'] . "' SELECTED>" . $employee_row['name'] . "</option>";
			}
			else
			{
				$employees .= "<option value='" . $employee_row['id'] . "'>" . $employee_row['name'] . "</option>";
			}
	
	}
	$employees .= "
				</select></td>
		</tr>";

	$query = "SELECT * FROM tasks WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	
	middle_top();
	echo "
	<form action='tasks.php?fnct=edit_task_complete&amp;id=$id' method='post'>
	<table>
		$employees
		<tr>
			<td align='right'><strong>Title:</strong></td><td align='left'><input type='text' name='title' maxlength='255' value='" . $row['title'] . "' /></td>
		</tr>
		<tr>
			<td align='right'><strong>Description:</strong></td><td align='left'><textarea cols='30' rows='3' name='description'>" . $row['description'] . "</textarea></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Save' /></td>
		</tr>
	</table>
	</form>";

	middle_bottom();
}

function edit_task_complete()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];
	$title = $_REQUEST['title'];
	$description = $_REQUEST['description'];
	
	$query = "UPDATE tasks
		SET
			emp_id = '$emp_id',
			title = '$title',
			description = '$description'
		WHERE
			id = $id";
	$result = @mysql_query($query);
	header("Location: tasks.php?fnct=view_task&id=$emp_id");
}

function delete_task()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];	
	
	middle_top();
	echo "
	<table>
		<tr>
			<td colspan='2' align='center'>Are you sure you want to delete this task <u>forever</u>?</td>
		</tr>
		<tr>
			<td><form action='tasks.php?fnct=delete_task_complete&amp;id=$id&amp;emp_id=$emp_id' method='post'><input type='submit' value='Yes' /></form></td><td><form action='tasks.php' method='post'><input type='submit' value='No' /></form></td>
		</tr>	
	</table>";

	middle_bottom();
	
}

function delete_task_complete()
{
	$id = $_REQUEST['id'];
	$emp_id = $_REQUEST['emp_id'];	

	$query = "DELETE FROM tasks WHERE id = $id";
	$result = @mysql_query($query);

	if(!empty($emp_id))
	{
		header("Location: tasks.php?fnct=view_task&id=$emp_id");
	}
	else
	{
		header("Location: tasks.php");
	}
}


function add_note_complete()
{
	$id = $_REQUEST['id'];
	$value = $_REQUEST['value'];
	$query = "INSERT INTO task_notes
		SET
			task_id = '$id',
			value = '$value'";
	$result = @mysql_query($query);
	header("Location:tasks.php?fnct=view_task_content&id=$id");

}

function edit_note()
{
	$id = $_REQUEST['id'];
	$task_id = $_REQUEST['task_id'];

	$query = "SELECT * FROM task_notes WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
?>
<form action='tasks.php?fnct=edit_note_complete&amp;id=<?php echo $id; ?>&amp;task_id=<?php echo $task_id; ?>' method='post' onsubmit='return submitForm();'>
				<script language="JavaScript" type="text/javascript">
					<!--
					function submitForm() {
					//make sure hidden and iframe values are in sync before submitting form
					//to sync only 1 rte, use updateRTE(rte)
					//to sync all rtes, use updateRTEs
					updateRTE('value');
					//updateRTEs();
					//change the following line to true to submit form
					return true;
					}
					//Usage: initRTE(imagesPath, includesPath, cssFile, genXHTML)
					initRTE("images/", "", "", true);
					//-->
					</script><noscript><p><b>Javascript must be enabled to use this form.</b></p></noscript>
		<?php
		echo "<script language='JavaScript' type='text/javascript'>";
		//format content for preloading
		/*if (!(isset($row['info']))) {
			$content = "here's the " . chr(13) . "\"preloaded <b>content</b>\"";
			$content = rteSafe($content);
		} else {
			//retrieve posted value
			*/
			$content = rteSafe($row['value']);
		//}
		//Usage: writeRichText(fieldname, html, width, height, buttons, readOnly)
		?>
					
		writeRichText('value', '<?=$content;?>', 530, 330, true, false);
		</script>
		<input type='submit' value='Save' />
</form>

<?php

}

function edit_note_complete()
{
	$id = $_REQUEST['id'];
	$value = $_REQUEST['value'];
	$task_id = $_REQUEST['task_id'];
	$query = "UPDATE task_notes
		SET
			value = '$value'
		WHERE
			id = $id";
	$result = @mysql_query($query);

	header("Location:tasks.php?fnct=view_task_content&id=$task_id");
}

function delete_note()
{
	$id = $_REQUEST['id'];
	$task_id = $_REQUEST['task_id'];
	echo "
	<table>
		<tr>
			<td colspan='2' align='left'>Are you sure you want to delete this Task Note <u>forever</u>?</td>
		</tr>
		<tr>
			<td align='left'><form action='tasks.php?fnct=delete_note_complete&amp;id=$id&amp;task_id=$task_id' method='post'><input type='submit' value='Yes' /></form></td><td align='right'><form action='tasks.php?fnct=view_task_content&id=2' method='post'><input type='submit' value='No' /></form></td>
		</tr>
	</table>";
	
}

function delete_note_complete()
{
	$id = $_REQUEST['id'];
	$task_id = $_REQUEST['task_id'];
	$query = "DELETE FROM task_notes WHERE id = $task_id";
	$result = @mysql_query($query);

	header("Location:tasks.php?fnct=view_task_content&id=$id");
}


?>

<?php include("includes/header.php"); ?>
<table width="750" border="0" cellpadding="0" cellspacing="10" style="height:400px;">
      <tr>
        <td width="200" valign="top">
		<table width="189" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td><img src="images/top_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
          <tr>
            <td bgcolor="#F6FAFE" style="border-right:1px solid #C2DDFA;border-left:1px solid #C2DDFA;padding-left:20px;padding-right:20px;" valign="top"> 
				<a href="tasks.php">All Tasks</a><br />
				<a href="tasks.php?fnct=complete_tasks">Completed Tasks</a><br />
			</td>
          </tr>
          <tr>
            <td><img src="images/bottom_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
        </table>
		</td>
        <td valign="top">


<?php

switch($fnct)
{
	case 'view_task':
	view_task();
	break;
	
	case 'view_task_content':
	view_task_content();
	break;
	
	case 'complete_tasks':
	complete_tasks();
	break;
	
	case 'view_completed_task':
	view_completed_task();
	break;
	
	case 'add_note_complete':
	add_note_complete();
	break;
	
	case 'edit_note':
	edit_note();
	break;
	
	case 'edit_note_complete':
	edit_note_complete();
	break;
	
	case 'delete_note':
	delete_note();
	break;
	
	case 'delete_note_complete':
	delete_note_complete();
	break;	
	
	case 'add_task':
	add_task();
	break;
	
	case 'add_task_complete':
	add_task_complete();
	break;
	
	case 'edit_task':
	edit_task();
	break;
	
	case 'edit_task_complete':
	edit_task_complete();
	break;
	
	case 'delete_task':
	delete_task();
	break;
	
	case 'delete_task_complete':
	delete_task_complete();
	break;

	default:
	home();
	break;
}

?>
		</td>
      </tr>
    </table>
<?php include("includes/footer.php"); ?>

<?php
function rteSafe($strText) {
	//returns safe code for preloading in the RTE
	$tmpString = $strText;
	
	//convert all types of single quotes
	$tmpString = str_replace(chr(145), chr(39), $tmpString);
	$tmpString = str_replace(chr(146), chr(39), $tmpString);
	$tmpString = str_replace("'", "&#39;", $tmpString);
	//$tmpString = str_replace(";", "&#59;", $tmpString);
	//$tmpString = str_replace("<", "&#60;", $tmpString);
	//$tmpString = str_replace(">", "&#62;", $tmpString);	
	
	//convert all types of double quotes
	$tmpString = str_replace(chr(147), chr(34), $tmpString);
	$tmpString = str_replace(chr(148), chr(34), $tmpString);
//	$tmpString = str_replace("\"", "\"", $tmpString);
	
	//replace carriage returns & line feeds
	$tmpString = str_replace(chr(10), " ", $tmpString);
	$tmpString = str_replace(chr(13), " ", $tmpString);
	
	return $tmpString;
}
?>