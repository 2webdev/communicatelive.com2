<?php include('adminlogin.php'); ?>

<?php $fnct = $_REQUEST['fnct'];

if($fnct == '')
{	
	$fnct = "home";
}

$section_name = "Gallery";
?>

<?php include("includes/head.php"); ?>

<?php

function middle_top()
{
	echo "<table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"15\"><img src=\"table/top_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td width=\"510\" style=\"background-image:url('table/top.gif');\"></td>
            <td width=\"15\"><img src=\"table/top_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
          <tr>
            <td width=\"15\" style=\"background-image:url('table/left.gif');\"></td>
            <td bgcolor=\"#F9F9F9\">";
}

function middle_bottom()
{
	echo "
				<p>&nbsp;</p>
			</td>
            <td width=\"15\" style=\"background-image:url('table/right.gif');\"></td>
          </tr>
          <tr>
            <td><img src=\"table/bottom_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td style=\"background-image:url('table/bottom.gif');\"></td>
            <td><img src=\"table/bottom_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
        </table>";
}


function home($site_name,$section_name)
{
	middle_top();

	echo "
	<table cellspacing='0' cellpadding='5' width='100%'>
		<tr>
			<td align='left'><b>Page Name</b></td><td><b>Gallery</b></td>
		</tr>
	";

	$query = "SELECT * FROM page WHERE ref_id = '' ORDER BY pos";
	$result = @mysql_query($query);
	while($row = @mysql_fetch_array($result))
	{
		echo "
		<tr>
			<td bgcolor='#ececec'>" . $row['name'] . "</td><td bgcolor='#ececec'><a href='gallery.php?fnct=show_gallery&amp;id=" . $row['id'] . "'>Gallery</a></td>
		</tr>";
		$query_sub = "SELECT * FROM page WHERE ref_id = '" . $row['id'] . "' ORDER BY pos";
		$result_sub = @mysql_query($query_sub);
		while($row_sub = @mysql_fetch_array($result_sub))
		{
		echo "
		<tr>
			<td bgcolor='#FFFFCC'>&nbsp;" . $row_sub['name'] . "</td><td bgcolor='#FFFFCC'><a href='gallery.php?fnct=show_gallery&amp;id=" . $row_sub['id'] . "'>Gallery</a></td>
		</tr>";
		}
	}

	echo "
	</table>";
	
	middle_bottom();
}

function show_gallery($site_name,$section_name)
{
	$pageid = $_REQUEST['id'];
	$query = "SELECT * FROM gallery WHERE pageid = $pageid";
	$result = @mysql_query($query);
	
	middle_top();
	echo "
	<table>
		<tr>
			<td colspan='2' align='left'><form action='gallery.php?fnct=add_image&amp;pageid=$pageid' method='post'><input type='submit' value='Add Image' /></form></td>
		</tr>";
		
	if(@mysql_num_rows($result) > 0)
	{
	echo "	
		<tr>
			<td align='left'><b>&nbsp;Image</b></td><td align='left'><b>Edit</b></td><td align='left'><b>Delete</b></td>
		</tr>
	
	";
	}
	
	while($row = @mysql_fetch_array($result))
	{
	echo "
		<tr>
			<td align='left'><a href='../gallery/" . $row['filename'] . "' target='blank'><img src='../gallery/" . $row['filename'] . "' width='75' border='0' alt='' /></a></td>
			<td align='left'><a href='gallery.php?fnct=edit_image&amp;id=" . $row['id']. "&amp;pageid=$pageid'>Edit</a></td>
			<td align='left'><a href='gallery.php?fnct=delete_image&amp;id=" . $row['id']. "&amp;pageid=$pageid'>Delete</a></td>
		</tr>
	";
	}
	
	echo "
	</table>";
	middle_bottom();

}

function add_image($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	
	middle_top();
	
	echo "
	<form action='gallery.php?fnct=add_image_complete&amp;pageid=$pageid' method='post' enctype='multipart/form-data'>
	<table>
		<tr>
			<td align='right'><b>Image:</b></td>
			<td align='left'><input type='file' name='upload[]' /></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Add Image' /></td>
		</tr>
	</table>
	</form>";
	
	middle_bottom();
}

function add_image_complete($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	
	$query = "SELECT value FROM config WHERE name = 'site_location'";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	$site_location = $row['value'];

	for($i = 0; $i < count($_FILES['upload']); $i++) 
	{
		if ($_FILES['upload']['error'][$i] == UPLOAD_ERR_OK) 
		{
			$tempName = $_FILES['upload']['tmp_name'][$i];
			$fileName = $_FILES['upload']['name'][$i];
			$saveDirectory = $site_location . "gallery/";
			@move_uploaded_file($tempName, $saveDirectory . $fileName);
		} 
		else
		{
			$error = "error error<br />";
		}
	}
	
	$filename =  $_FILES['upload']['name'][0];
	
	$query = "INSERT INTO gallery
		SET
			pageid = '$pageid',
			filename = '$filename'";
	$result = @mysql_query($query);
	header("location:gallery.php?fnct=show_gallery&id=$pageid");
}

function edit_image($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	$id = $_REQUEST['id'];
	
	middle_top();

	$query = "SELECT * FROM gallery WHERE id = $id";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	
	echo "
	<form action='gallery.php?fnct=edit_image_complete&amp;id=$id&amp;pageid=$pageid' method='post' enctype='multipart/form-data'>
	<table>
		<tr>
			<td align='right' valign='top'><b>Current Image</b></td>
			<td><a href='../gallery/" . $row['filename'] . "' target='blank'><img src='../gallery/" . $row['filename'] . "' border='0' alt='' width='75' /></a><input type='hidden' name='current_image' value='" . $row['filename'] . "' /></td>
		</tr>
		<tr>
			<td align='right'><b>New Image</b></td>
			<td><input type='file' name='upload[]' /></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Save' /></td>
		</tr>	
	</table>
	</form>
	";

	middle_bottom();
}

function edit_image_complete($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	$id = $_REQUEST['id'];

	$image = $_REQUEST['current_image'];
	
	$query = "SELECT value FROM config WHERE name = 'site_location'";
	$result = @mysql_query($query);
	$row = @mysql_fetch_array($result);
	$site_location = $row['value'];
	
if($_FILES['upload']['size'][0] != 0)
{
	for($i = 0; $i < count($_FILES['upload']); $i++) 
	{
		if ($_FILES['upload']['error'][$i] == UPLOAD_ERR_OK) 
		{
			$tempName = $_FILES['upload']['tmp_name'][$i];
			$fileName = $_FILES['upload']['name'][$i];
			$saveDirectory = $site_location . "gallery/";
			@move_uploaded_file($tempName, $saveDirectory . $fileName);
		} 
		else
		{
			$error = "An error occurred uploading the gallery Image<br />";
		}
	}
	$image = $_FILES['upload']['name'][0];
}

	$query = "UPDATE gallery
		SET
			filename = '$image'
		WHERE
			id = $id";
	$result = @mysql_query($query);
	header("location:gallery.php?fnct=show_gallery&id=$pageid");
	
}

function delete_image($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	$id = $_REQUEST['id'];
		
	middle_top();

	echo "
	<table width='750' cellpadding='0' cellspacing='0'>
		<tr>
			<td colspan='3'>Are you sure you want to delete this Gallery Image <u><b>forever</b></u></td>
		</tr>
		<tr class='tablerow2'>
			<td align='right'><form action='gallery.php?fnct=delete_image_complete&amp;id=$id&amp;pageid=$pageid' method='post'><input type='submit' value='Yes' /></form></td>
			<td align='left'><form action='gallery.php?fnct=show_gallery&amp;id=$pageid' method='post'><input type='submit' value='No' /></form></td>
			<td width='350'>&nbsp;</td>
		</tr>
	</table>";
	
	middle_bottom();

}

function delete_image_complete($site_name,$section_name)
{
	$pageid = $_REQUEST['pageid'];
	$id = $_REQUEST['id'];
	
	$query = "SELECT filename FROM gallery WHERE id = $id";
	$result = @mysql_query($query);
	
	if(@mysql_num_rows($result) > 0)
	{
		$row = @mysql_fetch_array($result);
		$image = "../gallery/" . $row['filename'];
		
		$return = @unlink($image);
	}
	//remove the info from the db
	$query = "DELETE FROM gallery WHERE id = '$id'";
	$result = @mysql_query($query);

	
	header("location:gallery.php?fnct=show_gallery&id=$pageid");
}
?>

<?php include("includes/header.php"); ?>
<table width="750" border="0" cellpadding="0" cellspacing="10" style="height:400px;">
      <tr>
        <td width="200" valign="top">
		<table width="189" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td><img src="images/top_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
          <tr>
            <td bgcolor="#F6FAFE" style="border-right:1px solid #C2DDFA;border-left:1px solid #C2DDFA;padding-left:20px;padding-right:20px;" valign="top"> 
			<a href='gallery.php'>Gallery List</a>
			</td>
          </tr>
          <tr>
            <td><img src="images/bottom_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
        </table>
		</td>
        <td valign="top">

<?php
	switch($fnct)
	{
		case 'add_image':
		add_image($site_name,$section_name);
		break;
		
		case 'add_image_complete':
		add_image_complete($site_name,$section_name);
		break;

		case 'show_gallery':
		show_gallery($site_name,$section_name);
		break;

		case 'edit_image':
		edit_image($site_name,$section_name);
		break;

		case 'edit_image_complete':
		edit_image_complete($site_name,$section_name);
		break;

		case 'delete_image':
		delete_image($site_name,$section_name);
		break;
		
		case 'delete_image_complete':
		delete_image_complete($site_name,$section_name);
		break;

		default:
		home($site_name,$section_name);
		break;
	}
?>
		</td>
      </tr>
    </table>
<?php include("includes/footer.php"); ?>
