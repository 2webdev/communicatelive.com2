//custom_config.js - My custom configuration file
//in fckconfig.js add this

/* add this to your toolbar or to the default toolbar
 * ['Insert_Variables']
 */
FCKConfig.Plugins.Add( 'library') ;
FCKConfig.ToolbarSets['Default'] = [
	['Source'],
	['Cut','Copy','Paste','PasteText','PasteWord','-','SpellCheck'],
	['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
	'/',
	['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
	['OrderedList','UnorderedList','-','Outdent','Indent'],
	['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
	['Link','Unlink','Anchor'],
	['Library','Table','Rule','SpecialChar','PageBreak','UniversalKey'],
	'/',
	['Style','FontFormat','FontName','FontSize'],
	['TextColor','BGColor'],
	['About','-','FitWindow']
] ;
//custom_config.js - continued
/* Add our plugin to the plugins list.
    FCKConfig.Plugins.Add( pluginName, availableLanguages, pathToPlugin )
        pluginName: The plugin name. The plugin directory must match this name.
        availableLanguages: comman separated list of available language files.
        pathToPlugin: absolute path to the plugins directory (the directory
            that contains the directory for the plugin).
*/
// Change the default plugin path.
//FCKConfig.PluginsPath = FCKConfig.BasePath.substr(0, FCKConfig.BasePath.length - 7) ;
// Add the plugin from the default path.

