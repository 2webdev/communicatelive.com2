/* FCKCommands.RegisterCommand(commandName, command)
       commandName - Command name, referenced by the Toolbar, etc...
       command - Command object (must provide an Execute() function).
*/
// Register the related commands.
FCKCommands.RegisterCommand(
   'Library',
    new FCKDialogCommand(
        FCKLang['DlgMyFindTitle'],
        FCKLang['DlgMyFindTitle'],
        FCKConfig.PluginsPath + 'library/library.php', 340, 370));
// Create the "Find" toolbar button.
var oFindItem = new FCKToolbarButton('Library', FCKLang['DlgMyFindTitle']);
oFindItem.IconPath = FCKConfig.PluginsPath + 'library/jpg.gif' ;
// 'My_Find' is the name used in the Toolbar config.
FCKToolbarItems.RegisterItem( 'Library', oFindItem ) ;

