<?php include('adminlogin.php'); ?>

<?php

$fnct = $_REQUEST['fnct'];

if($fnct == '')
{	
	$fnct = "home";
}

$section_name = "Taxes";
?>
<?php include("includes/head.php"); ?>

<?php
function middle_top()
{
	echo "<table width=\"540\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
          <tr>
            <td width=\"15\"><img src=\"table/top_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td width=\"510\" style=\"background-image:url('table/top.gif');\"></td>
            <td width=\"15\"><img src=\"table/top_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
          <tr>
            <td width=\"15\" style=\"background-image:url('table/left.gif');\"></td>
            <td bgcolor=\"#F9F9F9\">";
}

function middle_bottom()
{
	echo "
				<p>&nbsp;</p>
			</td>
            <td width=\"15\" style=\"background-image:url('table/right.gif');\"></td>
          </tr>
          <tr>
            <td><img src=\"table/bottom_left.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
            <td style=\"background-image:url('table/bottom.gif');\"></td>
            <td><img src=\"table/bottom_right.gif\" width=\"15\" height=\"15\" alt=\"\"></td>
          </tr>
        </table>";
}

function home()
{
	middle_top();
	
	echo "
	<table>
		<tr>
			<td colspan='3'><form action='tax_bracket.php?fnct=add_tax' method='post'><input type='submit' value='Add Tax Bracket' /></form></td>
		</tr>
	
	";
	$query = "SELECT * FROM taxes ORDER BY id ASC";
	$result = @mysql_query($query);
	if(@mysql_num_rows($result) > 0)
	{
		while($row = @mysql_fetch_array($result))
		{
	echo"
		<tr>
			<td>" . $row['name'] . "</td>
			<td><a href='tax_bracket.php?fnct=edit_tax&amp;id=" . $row['id'] ."'>Edit</a></td>
			<td>[ <a href='tax_bracket.php?fnct=delete_tax&amp;id=" . $row['id'] ."'>X</a> ]</td>
		</tr>";	
		}
	}
	echo "
	</table>";
	middle_bottom();
}

function add_tax()
{
	middle_top();
	echo "
	<form action='tax_bracket.php?fnct=add_tax_complete' method='post'>
	<table>
		<tr>
			<td align='right'>Name:</td><td align='left'><input type='text' name='name' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'>CPP:</td><td align='left'><input type='text' name='cpp' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'>EI:</td><td align='left'><input type='text' name='ei' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'>Fed Tax:</td><td align='left'><input type='text' name='fit' maxlength='200' /></td>
		</tr>
		<tr>
			<td align='right'>Prov Tax:</td><td align='left'><input type='text' name='pit' maxlength='200' /></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Add' /></td>
		</tr>
	</table>
	</form>
	";
	middle_bottom();
}

function add_tax_complete()
{
	$name = $_REQUEST['name'];
	$cpp = $_REQUEST['cpp'];
	$ei = $_REQUEST['ei'];
	$fit = $_REQUEST['fit'];
	$pit = $_REQUEST['pit'];
	
	$query = "INSERT INTO taxes
	SET
		name = '$name',
		cpp = '$cpp',
		ei = '$ei',
		fit = '$fit',
		pit = '$pit'";
	$result = @mysql_query($query);

	header("Location:tax_bracket.php");
}

function edit_tax()
{
	middle_top();
	
	$id = $_REQUEST['id'];
	$query = "SELECT * FROM taxes WHERE id = $id";
	$result = @mysql_query($query); 
	$row = @mysql_fetch_array($result);

	echo "
	<form action='tax_bracket.php?fnct=edit_tax_complete&amp;id=$id' method='post'>
	<table>
		<tr>
			<td align='right'>Name:</td><td align='left'><input type='text' name='name' maxlength='200' value='" . $row['name'] . "' /></td>
		</tr>
		<tr>
			<td align='right'>CPP:</td><td align='left'><input type='text' name='cpp' maxlength='200' value='" . $row['cpp'] . "' /></td>
		</tr>
		<tr>
			<td align='right'>EI:</td><td align='left'><input type='text' name='ei' maxlength='200' value='" . $row['ei'] . "' /></td>
		</tr>
		<tr>
			<td align='right'>Fed Tax:</td><td align='left'><input type='text' name='fit' maxlength='200' value='" . $row['fit'] . "' /></td>
		</tr>
		<tr>
			<td align='right'>Prov Tax:</td><td align='left'><input type='text' name='pit' maxlength='200' value='" . $row['pit'] . "' /></td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input type='submit' value='Add' /></td>
		</tr>
	</table>
	</form>
	";
	
	middle_bottom();
}

function edit_tax_complete()
{
	$id = $_REQUEST['id'];
	$name = $_REQUEST['name'];
	$cpp = $_REQUEST['cpp'];
	$ei = $_REQUEST['ei'];
	$fit = $_REQUEST['fit'];
	$pit = $_REQUEST['pit'];
	
	$query = "UPDATE taxes
	SET
		name = '$name',
		cpp = '$cpp',
		ei = '$ei',
		fit = '$fit',
		pit = '$pit'
	WHERE
		id = '$id'";
	$result = @mysql_query($query);

	header("Location:tax_bracket.php");

}

function delete_tax()
{
	middle_top();
	
	$id = $_REQUEST['id'];
	echo "
	<table>
		<tr>
			<td colspan='2' align='left'>Are you sure you want to delete this Tax Bracket <i>forever</i>?</td>
		</tr>
		<tr>
			<td align='center'><form action='tax_bracket.php?fnct=delete_tax_complete&amp;id=$id' method='post'><input type='submit' value='Yes' /></form></td>
			<td align='center'><form action='tax_bracket.php' method='post'><input type='submit' value='No' /></form></td>
		</tr>
	
	</table>";

	middle_bottom();
}

function delete_tax_complete()
{
	$id = $_REQUEST['id'];
	$query = "DELETE FROM taxes WHERE id = $id";
	$result = @mysql_query($query);

	header("Location:tax_bracket.php");

}

?>


<?php include("includes/header.php"); ?>
<table width="750" border="0" cellpadding="0" cellspacing="10" style="height:400px;">
      <tr>
        <td width="200" valign="top">
		<table width="189" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td><img src="images/top_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
          <tr>
            <td bgcolor="#F6FAFE" style="border-right:1px solid #C2DDFA;border-left:1px solid #C2DDFA;padding-left:20px;padding-right:20px;" valign="top"> 
			<a href='staff.php'>Staff List</a><br />
			<a href="staff.php?fnct=add_staff">Add Staff</a><br />
			<a href="tax_bracket.php">Taxes</a>
			</td>
          </tr>
          <tr>
            <td><img src="images/bottom_box03.gif" width="189" height="12" alt="" border="0"></td>
          </tr>
        </table>
		</td>
        <td valign="top">
<?php

switch($fnct)
{
	case 'add_tax':
	add_tax();
	break;

	case 'add_tax_complete':
	add_tax_complete();
	break;
	
	case 'edit_tax':
	edit_tax();
	break;
	
	case 'edit_tax_complete':
	edit_tax_complete();
	break;	

	case 'delete_tax':
	delete_tax();
	break;
	
	case 'delete_tax_complete':
	delete_tax_complete();
	break;	

	default:
	home();
	break;
}

?>
		</td>
      </tr>
    </table>
<?php include("includes/footer.php"); ?>