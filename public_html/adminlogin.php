<?php ob_start();
include 'includes/header.php';
session_start();
include_once 'common.php';

$uid = isset($_POST['uid']) ? $_POST['uid'] : $_SESSION['uid'];
$pwd = isset($_POST['pwd']) ? $_POST['pwd'] : $_SESSION['pwd'];

if(!isset($uid)) 
{

echo "
<table width='100%'>
	<tr>
		<td valign='top'>$value</td>
		<td valign='top'>
";
  ?>

  <form method="post" action="<?=$_SERVER['PHP_SELF']?>">
    <table align="center">
		<tr>
			<td align="right" style="font-size:12px;">User ID: </td><td align="left"><input type="text" name="uid" size="8" /></td>
		</tr>
		<tr>
			<td align="right" style="font-size:12px;">Password: </td><td align="left"><input type="password" name="pwd" SIZE="8" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Log in" /></td>
		</tr>
	</table>
  </form>
  <br />
  <?php
 			echo "
		</td>
	</tr>
</table>";

 
  include 'includes/footer.php';
  exit;
}

$_SESSION['uid'] = $uid;
$_SESSION['pwd'] = $pwd;

function quote_smart($value)
{
   // Stripslashes
   if (get_magic_quotes_gpc()) {
       $value = stripslashes($value);
   }
   // Quote if not a number or a numeric string
   if (!is_numeric($value)) {
       $value = "'" . mysql_real_escape_string($value) . "'";
   }
   return $value;
}
$query = sprintf("SELECT * FROM admin WHERE userName = %s AND passWord=%s",
		   quote_smart($uid),
           quote_smart($pwd));

$result = mysql_query($query);
if (!$result) {
 // error('A database error occurred while checking your '.
 //       'login details.\\nIf this error persists, please '.
 //       'contact an administrator.');
}

if (mysql_num_rows($result) == 0) {
  unset($_SESSION['uid']);
  unset($_SESSION['pwd']);
  ?><br />
<span style="font-size:12px;">
 <strong>Access Denied</strong><br />
 <br />
 Your user ID or password is incorrect, or you are not a
     registered user on this site. <br />To try logging in again, click
     <a href="<?=$_SERVER['PHP_SELF']?>">here</a><br /><br />
</span>
  <?php
  include 'includes/footer.php';
  exit;
}

?>
